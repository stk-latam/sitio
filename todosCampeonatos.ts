interface Resultado {
  usuario: string;
  puntos: number;
  puesto: number;
}

export interface Campeonato {
  nombre: string;
  fecha: string;
  puedeRegistrarHasta: string;
  resultados?: Resultado[];
  informacion: string[];
}

export interface CampeonatoConUrl {
  nombre: string;
  fecha: string;
  puedeRegistrarHasta: string;
  resultados?: Resultado[];
  informacion: string[];
  url: string;
}

const campeonatos: Campeonato[] = [
  {
    nombre: 'Primer campeonato',
    fecha: new Date('2021-06-04T18:00:00-04:00').toISOString(),
    puedeRegistrarHasta: new Date('2021-06-04T18:00:00-04:00').toISOString(),
    informacion: ['en este torneo se jugaran todas las pistas del modo historia de SuperTuxKart', 'este torneo ya finalizo y estos son los resultados'],
    resultados: [
      {
        usuario: 'juanman',
        puesto: 14,
        puntos: 3,
      },
      {
        usuario: 'matahina',
        puesto: 13,
        puntos: 14,
      },
      {
        usuario: 'G.Kart',
        puesto: 12,
        puntos: 27,
      },
      {
        usuario: 'oliveiravp',
        puesto: 11,
        puntos: 32,
      },
      {
        usuario: 'NickerAban',
        puesto: 10,
        puntos: 80,
      },
      {
        usuario: 'UnsolvedCypher',
        puesto: 9,
        puntos: 101,
      },
      {
        usuario: 'anneke',
        puesto: 8,
        puntos: 139,
      },
      {
        usuario: 'nimeye',
        puesto: 7,
        puntos: 153,
      },
      {
        usuario: 'RowdyJoe',
        puesto: 6,
        puntos: 203,
      },
      {
        usuario: 'RX1',
        puesto: 5,
        puntos: 238,
      },
      {
        usuario: 'nascartux-2',
        puesto: 4,
        puntos: 256,
      },
      {
        usuario: 'fodoman',
        puesto: 3,
        puntos: 287,
      },
      {
        usuario: 'LLS',
        puesto: 2,
        puntos: 305,
      },
      {
        usuario: 'haenschen',
        puesto: 1,
        puntos: 484,
      },
    ],
  },
  {
    nombre: 'Segundo campeonato',
    fecha: new Date('2023-01-01').toISOString(),
    puedeRegistrarHasta: new Date('2025-01-01').toISOString(),
    informacion: ['hola numero dos', 'lalala'],
  },
];

export function urlParaCampeonato(campeonato: Campeonato): string {
  return campeonato.nombre
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .split(' ').join('-')
    .replace(/[^0-9a-z-]/gi, '')
    .toLowerCase();
}

const campeonatosConUrl = campeonatos.map((c) => ({ ...c, url: urlParaCampeonato(c) }));

export function proximoCampeonato(): CampeonatoConUrl | undefined {
  return campeonatosConUrl
    .filter((c) => new Date(c.fecha) > new Date())
    .sort((a, b) => (a.fecha < b.fecha ? -1 : 1))[0];
}

export default campeonatosConUrl;
