/* eslint-disable no-nested-ternary */
import { GetServerSidePropsContext, GetServerSidePropsResult } from 'next';
import { withTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Link from 'next/link';
import React from 'react';
import { Label, Button, Table } from 'semantic-ui-react';
import { getLocale } from '../../../localeHelper';
import campeonatos, { CampeonatoConUrl, urlParaCampeonato } from '../../../todosCampeonatos';

interface PaginaParaCampeonatoProps {
  campeonato: CampeonatoConUrl;
}

class PaginaParaCampeonato extends React.Component<PaginaParaCampeonatoProps> {
  render() {
    const { campeonato } = this.props;
    return (
      <>
        <h1>{campeonato.nombre}</h1>
        <Label
          content={new Date(campeonato.fecha).toLocaleString(undefined, { timeZoneName: 'short' })}
          color="blue"
          icon="calendar"
        />
        <br />
        <br />
        <h2>Información</h2>
        {campeonato.informacion.map((info) => <p>{info}</p>)}
        {new Date(campeonato.puedeRegistrarHasta) > new Date()
          && (
          <Link href={`/campeonatos/${campeonato.url}/register`}>
            <Button icon="sign in" positive content="Inscribirse" />
          </Link>
          )}
        {campeonato.resultados
        && (
          (
          <>
            <h2 id="resultados">Resultados</h2>
            <Table unstackable celled striped>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell content="Puesto" />
                  <Table.HeaderCell content="Usuario" />
                  <Table.HeaderCell content="Puntos" />
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {campeonato.resultados.sort((a, b) => (a.puesto === b.puesto
                  ? 0
                  : a.puesto < b.puesto
                    ? -1
                    : 1
                )).map((r) => (
                  <Table.Row key={r.puesto}>
                    <Table.Cell content={r.puesto} />
                    <Table.Cell content={r.usuario} />
                    <Table.Cell content={r.puntos} />
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
          </>
          )
        )}
      </>
    );
  }
}

export async function getServerSideProps(
  context: GetServerSidePropsContext,
): Promise<GetServerSidePropsResult<PaginaParaCampeonatoProps>> {
  const campeonato = campeonatos.find((c) => urlParaCampeonato(c) === context.params.id);
  if (campeonato == null) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      campeonato,
      ...(await serverSideTranslations(getLocale(context), ['common'])),
    },
  };
}

export default withTranslation('common')(PaginaParaCampeonato);
