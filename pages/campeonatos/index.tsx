import React from 'react';
import { Button, Table } from 'semantic-ui-react';
import { withTranslation } from 'next-i18next';
import { GetServerSideProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Link from 'next/link';
import campeonatos, { urlParaCampeonato } from '../../todosCampeonatos';
import { getLocale } from '../../localeHelper';

class Championships extends React.Component {
  render() {
    const campeonatosEnOrden = campeonatos.sort((a, b) => (a.fecha < b.fecha ? -1 : 1));
    const proximosCampeonatos = campeonatosEnOrden.filter((c) => new Date(c.fecha) > new Date());
    const campeonatosPasados = campeonatosEnOrden.filter((c) => new Date(c.fecha) < new Date());
    return (
      <>
        <h1>Campeonatos</h1>
        {proximosCampeonatos.length > 0
        && (
        <>
          <h2>Próximos campeonatos</h2>
          <Table celled striped unstackable>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell content="Nombre" />
                <Table.HeaderCell content="Fecha" />
                <Table.HeaderCell content="Inscripción" />
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {proximosCampeonatos
                .map((c) => (
                  <Table.Row key={c.nombre}>
                    <Table.Cell>
                      <Link href={`/campeonatos/${urlParaCampeonato(c)}`}>
                        <a>{c.nombre}</a>
                      </Link>
                    </Table.Cell>
                    <Table.Cell content={new Date(c.fecha).toLocaleString(undefined, { timeZoneName: 'short' })} />
                    <Table.Cell>
                      {new Date(c.puedeRegistrarHasta) > new Date()
                        ? (
                          <Link href={`/campeonatos/${urlParaCampeonato(c)}/register`}>
                            <Button primary content="Inscribirse" />
                          </Link>
                        )
                        : 'Inscripción cerrada'}
                    </Table.Cell>
                  </Table.Row>
                ))}
            </Table.Body>
          </Table>
        </>
        )}
        <h2>Campeonatos pasados</h2>
        <Table celled striped unstackable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell content="Nombre" />
              <Table.HeaderCell content="Fecha" />
              <Table.HeaderCell content="Resultados" />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {campeonatosPasados
              .map((c) => (
                <Table.Row key={c.nombre}>
                  <Table.Cell><Link href={`/campeonatos/${urlParaCampeonato(c)}`}><a>{c.nombre}</a></Link></Table.Cell>
                  <Table.Cell content={new Date(c.fecha).toLocaleString(undefined, { timeZoneName: 'short' })} />
                  <Table.Cell content={
                    c.resultados
                      ? (
                        <Link href={`/campeonatos/${urlParaCampeonato(c)}#resultados`}>
                          <a>Ver resultados</a>
                        </Link>
                      )
                      : 'Resultados no disponibles'
                  }
                  />
                </Table.Row>
              ))}
          </Table.Body>
        </Table>
      </>
    );
  }
}

export const getServerSideProps: GetServerSideProps = async (context) => ({
  props: {
    ...(await serverSideTranslations(getLocale(context), ['common'])),
  },
});
export default withTranslation('common')(Championships);
