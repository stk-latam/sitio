import React from 'react';
import {
  Header,
  Message,
  Table,
  Button,
} from 'semantic-ui-react';
import Link from 'next/link';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { GetServerSideProps } from 'next';
import { withTranslation, WithTranslation } from 'next-i18next';
import { getLocale } from '../localeHelper';
import NuevoCampeonatoBanner from '../NuevoCampeonatoBanner';

class Home extends React.Component<WithTranslation> {
  render(): React.ReactNode {
    const { t } = this.props;
    return (
      <>
        {/* <NuevoCampeonatoBanner /> */}
        <Header as="h1">STK Latinoamérica</Header>
        <p>
          {t('welcome')}
        </p>
        <p>
          {t('site_description_1')}
          {' '}
          <a href="https://supertuxkart.net">SuperTuxKart</a>
          {' '}
          {t('site_description_2')}
        </p>

        <Link href="/records"><Button primary content={t('records_here')} /></Link>
        <br />
        <br />
        <h3>Servidores</h3>
        <ul>
          <li>
            🇦🇷 STK Latinoamérica Argentina
            {' '}
            <a href="https://t.me/stk_latam_ar">
              (
              {t('telegram_channel')}
              )
            </a>
          </li>
          <li>
            🇧🇷 STK Latinoamérica Brasil
            {' '}
            <a href="https://t.me/stk_latam_br">
              (
              {t('telegram_channel')}
              )
            </a>
          </li>
        </ul>

        <h3>{t('rules')}</h3>
        <ul>
          <li>{t('be_nice_rule')}</li>
          <li>{t('no_spam_rule')}</li>
        </ul>
        <Message
          color="yellow"
          content={`⚠️ ${t('ban_warning')}`}
        />

        <h3>{t('features')}</h3>
        <p>
          {t('features_description_1')}
          {' '}
          <a href="https://stk.kimden.online">{t('kimdens_modifications')}</a>
          {' '}
          {t('features_description_2')}
        </p>

        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell content={t('command_header')} />
              <Table.HeaderCell content={t('command_result_header')} />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell content={`/music [${t('command_volume_param')}]`} />
              <Table.Cell content={t('command_volume_description')} />
            </Table.Row>
            <Table.Row>
              <Table.Cell content={`/kick [${t('command_player_param')}]`} />
              <Table.Cell content={t('command_kick_description')} />
            </Table.Row>
            <Table.Row>
              <Table.Cell content={`/installaddon [${t('command_addon_or_url_param')}]`} />
              <Table.Cell content={t('command_install_addon_or_url_description')} />
            </Table.Row>
            <Table.Row>
              <Table.Cell content={`/uninstalladdon [${t('command_addon_id_param')}]`} />
              <Table.Cell content={t('command_uninstall_addon_or_url_description')} />
            </Table.Row>
            <Table.Row>
              <Table.Cell content={`/liststkaddon [${t('command_addon_type_param')}] [${t('command_addon_name_param')}]`} />
              <Table.Cell content={t('command_list_stk_addons_description')} />
            </Table.Row>
            <Table.Row>
              <Table.Cell content={`/listlocaladdon [${t('command_addon_type_param')}] [${t('command_addon_name_param')}]`} />
              <Table.Cell content="Buscar en su computadora add-ons que empiezan con [numbre]. [Tipo] no es obligatorio, y puede ser -track, -arena, o -kart" />
            </Table.Row>
            <Table.Row>
              <Table.Cell content={`/listserveraddon [${t('command_addon_type_param')}] [${t('command_addon_name_param')}]`} />
              <Table.Cell content="Buscar en el servidor add-ons que empiezan con [numbre]. [Tipo] no es obligatorio, y puede ser -track, -arena, o -kart" />
            </Table.Row>
            <Table.Row>
              <Table.Cell content={`/playerhasaddon [${t('command_addon_id_param')}] [${t('command_player_param')}]`} />
              <Table.Cell content="Averiguar si el jugador tiene el addon con el add-id especificado" />
            </Table.Row>
            <Table.Row>
              <Table.Cell content={`/serverhasaddon [${t('command_addon_id_param')}]`} />
              <Table.Cell content="Averiguar si el servidor tiene el addon con el add-id especificado" />
            </Table.Row>
            <Table.Row>
              <Table.Cell content={`/playeraddonscore [${t('command_player_param')}]`} />
              <Table.Cell content="Ver el porcentage que tiene el jugador de todos los add-ons disponibles del servidor" />
            </Table.Row>
          </Table.Body>
        </Table>
        <h3>{t('credits')}</h3>
        <ul>
          <li>UnsolvedCypher</li>
          <li>G. Kart</li>
          <li>Fodoman</li>
        </ul>
      </>
    );
  }
}

export const getServerSideProps: GetServerSideProps = async (context) => ({
  props: {
    ...(await serverSideTranslations(getLocale(context), ['common'])),
  },
});

export default withTranslation('common')(Home);
