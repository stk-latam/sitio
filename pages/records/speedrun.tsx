/* eslint-disable jsx-a11y/label-has-associated-control */
import Link from 'next/link';
import React from 'react';
import {
  Table, Button, Form, Input,
} from 'semantic-ui-react';
import { GetServerSideProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { WithTranslation, withTranslation } from 'next-i18next';
import fetchUrl from '../../fetchWithUrl';
import { getLocale } from '../../localeHelper';

interface SpeedrunState {
  results: Array<any>;
  direction: 'reverse' | 'normal';
  withAddons: boolean;
  filterContent: string;
  trackNames: Array<string>;
  trackDefaultLaps: Array<number>;
  trackIds: Array<string>;
}

interface SpeedrunProps extends WithTranslation {
  speedrunResults: Array<any>;
  trackNames: Array<string>;
  trackIds: Array<string>;
  trackDefaultLaps: Array<number>;
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const res = await fetchUrl('/api/speedrun?direction=normal');
  const {
    speedrunResults, trackNames, trackIds, trackDefaultLaps,
  } = await res.json();
  return {
    props: {
      speedrunResults,
      trackNames,
      trackIds,
      trackDefaultLaps,
      ...(await serverSideTranslations(getLocale(context), ['common'])),
    },
  };
};

class SpeedrunPage extends React.Component<SpeedrunProps, SpeedrunState> {
  private firstScrollSyncing = false;

  private secondScrollSyncing = false;

  constructor(props: SpeedrunProps) {
    super(props);
    this.state = {
      results: props.speedrunResults,
      direction: 'normal',
      filterContent: '',
      withAddons: false,
      trackNames: props.trackNames,
      trackIds: props.trackIds,
      trackDefaultLaps: props.trackDefaultLaps,
    };
  }

  static getBackgroundColor(ranks: Array<number>, index: number): string {
    const asPercent = Math.min(1, ranks[index] / 25);
    const h = asPercent * 240;
    return `hsl(${h}, 100%, 50%, 0.6)`;
  }

  componentDidMount(): void {
    const wrapper1 = document.getElementById('wrapper1');
    const wrapper2 = document.getElementById('wrapper2');
    wrapper1.onscroll = () => {
      if (!this.secondScrollSyncing) {
        this.firstScrollSyncing = true;
        wrapper2.scrollLeft = wrapper1.scrollLeft;
      }
      this.secondScrollSyncing = false;
    };

    wrapper2.onscroll = () => {
      if (!this.firstScrollSyncing) {
        this.secondScrollSyncing = true;
        wrapper1.scrollLeft = wrapper2.scrollLeft;
      }
      this.firstScrollSyncing = false;
    };

    const div1 = document.getElementById('div1');
    const div2 = document.getElementById('div2');
    div1.style.width = getComputedStyle(div2).width;
    wrapper1.style.width = getComputedStyle(wrapper2).width;
  }

  updateRecords = async (): Promise<void> => {
    this.setState({ results: [] });
    const { direction, withAddons } = this.state;
    const params = new URLSearchParams();
    params.append('direction', direction);
    if (withAddons) {
      params.append('addonsMode', 'true');
    }
    const res = await fetchUrl(`/api/speedrun?${params}`);
    const json = await res.json();
    this.setState({
      results: json.speedrunResults,
      trackIds: json.trackIds,
      trackNames: json.trackNames,
      trackDefaultLaps: json.trackDefaultLaps,
    });
  };

  render(): React.ReactNode {
    const { t } = this.props;
    const {
      trackNames, direction, results, filterContent, withAddons, trackDefaultLaps, trackIds,
    } = this.state;
    return (
      <>
        <style jsx>
          {`
          .ui.table thead {
            position: sticky;
            position: -webkit-sticky;
            top: 0;
            z-index: 1;
        }
        `}
        </style>
        <h1>{t('speedrun')}</h1>
        <p>{t('speedrun_description')}</p>
        <i>{t('speedrun_warning')}</i>
        <br />
        <br />
        <Form>
          <Form.Field>
            <label>{t('direction')}</label>
            <Button.Group>
              <Button
                content={t('official_tracks')}
                positive={!withAddons}
                onClick={() => this.setState({ withAddons: false }, this.updateRecords)}
              />
              <Button
                content={t('addon_tracks')}
                positive={withAddons}
                onClick={() => this.setState({ withAddons: true }, this.updateRecords)}
              />
            </Button.Group>
          </Form.Field>
          <Form.Field>
            <Button.Group>
              <Button
                content={t('normal_direction')}
                positive={direction === 'normal'}
                onClick={() => this.setState({ direction: 'normal' }, this.updateRecords)}
              />
              <Button
                content={t('reverse_direction')}
                positive={direction === 'reverse'}
                onClick={() => this.setState({ direction: 'reverse' }, this.updateRecords)}
              />
            </Button.Group>
          </Form.Field>
          <br />
          <Input
            style={{ minWidth: '50%' }}
            icon={{ name: 'search', circular: true, link: true }}
            placeholder={t('filter')}
            onChange={(e) => this.setState({ filterContent: e.target.value })}
          />
        </Form>
        <br />
        <div id="wrapper1">
          <div id="div1" />
        </div>
        <div id="wrapper2">
          <div id="div2" style={{ overflowX: 'scroll', minHeight: '100%' }}>
            <Table celled striped unstackable>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell content={t('place')} />
                  <Table.HeaderCell content={t('user')} />
                  <Table.HeaderCell content={t('points')} />
                  {trackIds.map((trackId, trackIndex) => (
                    <Table.HeaderCell key={trackId}>
                      <Link
                        href={`/records/[trackId]?laps=${trackDefaultLaps[trackIndex]}&mode=timetrial&direction=normal&server=any`}
                        as={`/records/${trackId}?laps=${trackDefaultLaps[trackIndex]}&mode=timetrial&direction=normal&server=any`}
                      >
                        <a>{trackNames[trackIndex]}</a>
                      </Link>
                    </Table.HeaderCell>
                  ))}
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {results
                  .filter((r) => (filterContent === '' ? true : r.username.toLowerCase().includes(filterContent.toLowerCase())))
                  .map((r) => (
                    <Table.Row key={`${r.username}:${r.direction}`}>
                      <Table.Cell content={r.overallRank} />
                      <Table.Cell content={r.username} />
                      <Table.Cell content={r.sumOfRanks} />
                      {trackIds.map((trackId, trackIndex) => (
                        <Table.Cell
                          key={trackId}
                          style={{
                            backgroundColor: SpeedrunPage.getBackgroundColor(r.ranks, trackIndex),
                          }}
                          content={r.ranks[trackIndex]}
                        />
                      ))}
                    </Table.Row>
                  ))}
              </Table.Body>
            </Table>
          </div>
        </div>
        <br />
      </>
    );
  }
}

export default withTranslation('common')(SpeedrunPage);
