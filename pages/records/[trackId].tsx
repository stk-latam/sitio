/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import { Table, Button, Form } from 'semantic-ui-react';
import { GetServerSideProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { withTranslation } from 'react-i18next';
import { WithTranslation } from 'next-i18next';
import { parseResult } from '../../apiHelper';
import fetchUrl from '../../fetchWithUrl';
import { getLocale } from '../../localeHelper';

interface Record {
  recordHolder: string;
  rank: string;
  record: string;
  numLaps;
  direction: 'normal' | 'reverse';
}

interface RecordsState {
  records: Array<Record>;
  direction: 'reverse' | 'normal' | 'all';
  mode: string;
  laps: string;
  server: string;
}

interface RecordsProps extends WithTranslation {
  initialRecords: Array<Record>;
  trackId: string;
  trackInfo: any;
  mode: string;
  laps: string;
  server: string;
  direction: 'reverse' | 'normal' | 'all';
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context;
  const res = await fetchUrl(`/api/track-records?laps=${query.laps}&direction=${query.direction}&trackId=${query.trackId}&mode=${query.mode}&region=${query.server}`);
  const initialRecords = await res.json();
  return {
    props: {
      initialRecords: initialRecords.records,
      trackId: query.trackId,
      trackInfo: initialRecords.trackInfo,
      laps: query.laps,
      direction: query.direction,
      mode: query.mode,
      server: query.server,
      ...(await serverSideTranslations(getLocale(context), ['common'])),
    },
  };
};

class RecordsPage extends React.Component<RecordsProps, RecordsState> {
  constructor(props: RecordsProps) {
    super(props);
    this.state = {
      laps: props.laps,
      mode: props.mode,
      server: props.server,
      direction: props.direction,
      records: props.initialRecords,
    };
  }

  updateRecords = async () => {
    this.setState({ records: [] });
    const {
      laps, server, direction, mode,
    } = this.state;
    const params = new URLSearchParams();
    params.append('trackId', this.props.trackId);
    params.append('laps', laps);
    params.append('direction', direction);
    params.append('mode', mode);
    params.append('region', server);
    // eslint-disable-next-line no-template-curly-in-string
    const res = await fetchUrl(`/api/track-records?${params}`);
    const records = await res.json();
    this.setState({ records: records.records });
  };

  render() {
    const { trackInfo, t } = this.props;
    const {
      records, laps, direction, server, mode,
    } = this.state;
    return (
      <>
        <h1>
          Récords para
          {' '}
          {trackInfo.name}
        </h1>
        <Form>
          <Form.Field>
            <label>{t('laps')}</label>
            <Button.Group>
              <Button
                content={t('all_laps')}
                positive={laps === '0'}
                onClick={() => this.setState({ laps: '0' }, this.updateRecords)}
              />
              {[1, 2, 3, 4].map((n) => (
                <Button
                  key={n}
                  content={`${n}`}
                  positive={laps === `${n}`}
                  onClick={() => this.setState({ laps: `${n}` }, this.updateRecords)}
                />
              ))}
              <Button
                content={t('5_or_more_laps')}
                positive={laps === '5'}
                onClick={() => this.setState({ laps: '5' }, this.updateRecords)}
              />
            </Button.Group>
          </Form.Field>
          <Form.Field>
            <label>{t('direction')}</label>
            <Button.Group>
              <Button
                content={t('normal_direction')}
                positive={direction === 'normal'}
                onClick={() => this.setState({ direction: 'normal' }, this.updateRecords)}
              />
              <Button
                content={t('reverse_direction')}
                positive={direction === 'reverse'}
                onClick={() => this.setState({ direction: 'reverse' }, this.updateRecords)}
              />
              <Button
                content={t('both_directions')}
                positive={direction === 'all'}
                onClick={() => this.setState({ direction: 'all' }, this.updateRecords)}
              />
            </Button.Group>
          </Form.Field>

          <Form.Field>
            <label>{t('servers')}</label>
            <Button.Group>
              <Button
                content={t('any_server')}
                positive={server === 'any'}
                onClick={() => this.setState({ server: 'any' }, this.updateRecords)}
              />
              <Button
                content={t('argentina_servers')}
                positive={server === 'AR'}
                onClick={() => this.setState({ server: 'AR' }, this.updateRecords)}
              />
              <Button
                content={t('brasil_servers')}
                positive={server === 'BR'}
                onClick={() => this.setState({ server: 'BR' }, this.updateRecords)}
              />
              <Button
                content={t('miami_servers')}
                positive={server === 'MI'}
                onClick={() => this.setState({ server: 'MI' }, this.updateRecords)}
              />
            </Button.Group>
          </Form.Field>
          <Form.Field>
            <label>{t('mode')}</label>
            <Button.Group>
              <Button
                content={t('any_mode')}
                positive={mode === 'any'}
                onClick={() => this.setState({ mode: 'any' }, this.updateRecords)}
              />
              <Button
                content={t('normal_mode')}
                positive={mode === 'normal'}
                onClick={() => this.setState({ mode: 'normal' }, this.updateRecords)}
              />
              <Button
                content={t('time_trial_mode')}
                positive={mode === 'timetrial'}
                onClick={() => this.setState({ mode: 'timetrial' }, this.updateRecords)}
              />
            </Button.Group>
          </Form.Field>
        </Form>
        <br />
        <Table celled striped unstackable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell content={t('place')} />
              <Table.HeaderCell content={t('user')} />
              <Table.HeaderCell content={t('laps')} />
              <Table.HeaderCell content={t('direction')} />
              <Table.HeaderCell content={t('result')} />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {records.map((r) => (
              <Table.Row key={`${r.recordHolder}:${r.numLaps}:${r.direction}`}>
                <Table.Cell content={r.rank} />
                <Table.Cell content={r.recordHolder} />
                <Table.Cell content={r.numLaps} />
                <Table.Cell content={r.direction === 'normal' ? 'normal' : 'al revés'} />
                <Table.Cell content={parseResult(r.record)} />
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
        <br />
      </>
    );
  }
}

export default withTranslation('common')(RecordsPage);
