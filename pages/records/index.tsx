/* eslint-disable jsx-a11y/label-has-associated-control */
import Link from 'next/link';
import React from 'react';
import {
  Table, Button, Form, Input,
} from 'semantic-ui-react';
import { GetServerSideProps } from 'next';
import { WithTranslation, withTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import fetchUrl from '../../fetchWithUrl';
import { getLocale } from '../../localeHelper';

interface Track {
  name: string;
  id: string;
  record: string;
  recordHolder: string;
  isAddon: boolean;
  numLaps;
  direction: 'normal' | 'reverse';
  time: string;
}

interface RecordsState {
  records: Array<Track>;
  numLaps: number;
  direction: 'reverse' | 'normal' | 'all';
  filterContent: string;
  server: string;
  mode: string;
}

interface RecordsProps extends WithTranslation {
  initialRecords: Array<Track>;
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const res = await fetchUrl('/api/tracks?laps=3&direction=normal&mode=normal&region=any');
  const initialRecords = await res.json();
  return {
    props: {
      initialRecords: initialRecords.records,
      ...(await serverSideTranslations(getLocale(context), ['common'])),
    },
  };
};

class RecordsPage extends React.Component<RecordsProps, RecordsState> {
  constructor(props: RecordsProps) {
    super(props);
    this.state = {
      numLaps: 3,
      direction: 'normal',
      records: props.initialRecords,
      filterContent: '',
      server: 'any',
      mode: 'any',
    };
  }

  updateRecords = async (): Promise<void> => {
    this.setState({ records: [] });
    const {
      numLaps, direction, server, mode,
    } = this.state;
    const params = new URLSearchParams();
    params.append('laps', `${numLaps}`);
    params.append('direction', direction);
    params.append('region', server);
    params.append('mode', mode);
    const res = await fetchUrl(`/api/tracks?${params}`);
    const records = await res.json();
    this.setState({ records: records.records });
  };

  render(): React.ReactNode {
    const { t } = this.props;
    const {
      records, numLaps, direction, filterContent, server, mode,
    } = this.state;
    return (
      <>
        <h1>{t('server_records')}</h1>
        <Form>
          <Form.Field>
            <label>{t('laps')}</label>
            <Button.Group>
              <Button
                content={t('all_laps')}
                positive={numLaps === 0}
                onClick={() => this.setState({ numLaps: 0 }, this.updateRecords)}
              />
              {[1, 2, 3, 4].map((n) => (
                <Button
                  key={n}
                  content={`${n}`}
                  positive={numLaps === n}
                  onClick={() => this.setState({ numLaps: n }, this.updateRecords)}
                />
              ))}
              <Button
                content={t('5_or_more_laps')}
                positive={numLaps >= 5}
                onClick={() => this.setState({ numLaps: 5 }, this.updateRecords)}
              />
            </Button.Group>
          </Form.Field>
          <Form.Field>
            <label>{t('direction')}</label>
            <Button.Group>
              <Button
                content={t('normal_direction')}
                positive={direction === 'normal'}
                onClick={() => this.setState({ direction: 'normal' }, this.updateRecords)}
              />
              <Button
                content={t('reverse_direction')}
                positive={direction === 'reverse'}
                onClick={() => this.setState({ direction: 'reverse' }, this.updateRecords)}
              />
              <Button
                content={t('both_directions')}
                positive={direction === 'all'}
                onClick={() => this.setState({ direction: 'all' }, this.updateRecords)}
              />
            </Button.Group>
          </Form.Field>
          <Form.Field>
            <label>{t('servers')}</label>
            <Button.Group>
              <Button
                content={t('any_server')}
                positive={server === 'any'}
                onClick={() => this.setState({ server: 'any' }, this.updateRecords)}
              />
              <Button
                content={t('argentina_servers')}
                positive={server === 'AR'}
                onClick={() => this.setState({ server: 'AR' }, this.updateRecords)}
              />
              <Button
                content={t('brasil_servers')}
                positive={server === 'BR'}
                onClick={() => this.setState({ server: 'BR' }, this.updateRecords)}
              />
              <Button
                content={t('miami_servers')}
                positive={server === 'MI'}
                onClick={() => this.setState({ server: 'MI' }, this.updateRecords)}
              />
            </Button.Group>
          </Form.Field>
          <Form.Field>
            <label>{t('mode')}</label>
            <Button.Group>
              <Button
                content={t('any_mode')}
                positive={mode === 'any'}
                onClick={() => this.setState({ mode: 'any' }, this.updateRecords)}
              />
              <Button
                content={t('normal_mode')}
                positive={mode === 'normal'}
                onClick={() => this.setState({ mode: 'normal' }, this.updateRecords)}
              />
              <Button
                content={t('time_trial_mode')}
                positive={mode === 'timetrial'}
                onClick={() => this.setState({ mode: 'timetrial' }, this.updateRecords)}
              />
            </Button.Group>
          </Form.Field>
        </Form>
        <br />
        <Input
          style={{ minWidth: '50%' }}
          icon={{ name: 'search', circular: true, link: true }}
          placeholder={t('filter')}
          onChange={(e) => this.setState({ filterContent: e.target.value })}
        />
        <br />
        <h3>{t('official_tracks')}</h3>
        <Table celled striped unstackable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell content={t('track')} />
              <Table.HeaderCell content={t('laps')} />
              <Table.HeaderCell content={t('direction')} />
              <Table.HeaderCell content={t('user')} />
              <Table.HeaderCell content={t('result')} />
              <Table.HeaderCell content={t('date')} />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {records.filter((r) => !r.isAddon && (filterContent === '' || r.name.toLowerCase().includes(filterContent))).map((r) => (
              <Table.Row key={`${r.id}:${r.numLaps}:${r.direction}`}>
                <Table.Cell>
                  <Link
                    href={`/records/${r.id}?laps=${numLaps}&direction=${direction}&mode=${mode}&server=${server}`}
                  >
                    <a>{r.name}</a>
                  </Link>
                </Table.Cell>
                <Table.Cell content={r.numLaps} />
                <Table.Cell content={r.direction} />
                <Table.Cell content={r.recordHolder} />
                <Table.Cell content={r.record} />
                <Table.Cell content={r.time} />
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
        <br />
        <h3>{t('addon_tracks')}</h3>
        <Table celled striped unstackable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell content={t('track')} />
              <Table.HeaderCell content={t('laps')} />
              <Table.HeaderCell content={t('direction')} />
              <Table.HeaderCell content={t('user')} />
              <Table.HeaderCell content={t('result')} />
              <Table.HeaderCell content={t('date')} />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {records.filter((r) => r.isAddon && (filterContent === '' || r.name.toLowerCase().includes(filterContent))).map((r) => (
              <Table.Row key={`${r.id}:${r.numLaps}:${r.direction}`}>
                <Table.Cell>
                  <Link
                    href={`/records/${r.id}?laps=${numLaps}&direction=${direction}&mode=${mode}&server=${server}`}
                  >
                    <a>{r.name}</a>
                  </Link>
                </Table.Cell>
                <Table.Cell content={r.numLaps} />
                <Table.Cell content={r.direction} />
                <Table.Cell content={r.recordHolder} />
                <Table.Cell content={r.record} />
                <Table.Cell content={r.time} />
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </>
    );
  }
}

export default withTranslation('common')(RecordsPage);
