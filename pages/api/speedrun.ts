import sqlite3 from 'sqlite3';
import { open } from 'sqlite';
import SQL from 'sql-template-strings';
import fs from 'fs/promises';
import getTrackInfo from '../../getTrackInfo';

interface UserInfo {
  sumOfRanks: number | null;
  ranks: Array<number | undefined>;
}

async function processTrackSpeedrunInfo(db: any, userMap: Map<string, UserInfo>,
  trackIndex: number, trackId: string, numLaps: string, direction: string) {
  const query = SQL`SELECT username, min(result), row_number() over(ORDER BY result ASC) AS rank FROM `.append(process.env.allTable).append(SQL` WHERE laps = ${numLaps} AND venue = ${trackId} AND reverse = ${direction} AND mode = "time-trial" GROUP BY username ORDER BY result ASC LIMIT 500`);

  const results = await db.all(query);

  results.forEach((result) => {
    if (!userMap.has(result.username)) {
      userMap.set(result.username, {
        sumOfRanks: null,
        ranks: [],
      });
    }
    const userRanks = userMap.get(result.username).ranks;
    userRanks[trackIndex] = result.rank;
  });
  return results.length + 1;
}

async function getTracksForSpeedrun(addonsMode: boolean, db: any) {
  if (addonsMode) {
    const addonsFile = await fs.readFile(process.env.speedrunAddonsFile);
    return addonsFile.toString().trim().split(' ');
  }
  const allTracksWithAddons = (await db.all(SQL`SELECT distinct venue FROM `.append(process.env.allTable).append(SQL` ORDER BY venue ASC`))).map((x) => x.venue);
  return allTracksWithAddons.filter((result) => result && !result.startsWith('addon_'));
}

export default async (req, res) => {
  const {
    query: { direction, addonsMode },
  } = req;

  const db = await open({
    filename: process.env.dbLocation,
    driver: sqlite3.Database,
  });

  res.statusCode = 200;
  const allTracks = await getTracksForSpeedrun(addonsMode, db);
  const trackNames = [];
  const trackDefaultLaps = [];
  const rankingsForPlayersWithoutResults: number[] = [];
  const userMap: Map<string, UserInfo> = new Map();
  for (let trackIndex = 0; trackIndex < allTracks.length; trackIndex += 1) {
    const trackId = allTracks[trackIndex];
    const trackInfo = await getTrackInfo(trackId, trackId.startsWith('addon_'));
    trackNames.push(trackInfo.name);
    trackDefaultLaps.push(trackInfo.defaultLaps);
    rankingsForPlayersWithoutResults.push(
      await processTrackSpeedrunInfo(db, userMap, trackIndex, trackId,
        trackInfo.defaultLaps, direction),
    );
  }

  const rankingsTable = [];

  userMap.forEach((userInfo, username) => {
    const newRanks = [];
    for (let trackIndex = 0; trackIndex < allTracks.length; trackIndex += 1) {
      newRanks.push(userInfo.ranks[trackIndex] === undefined
        ? rankingsForPlayersWithoutResults[trackIndex]
        : userInfo.ranks[trackIndex]);
    }

    const newsumOfRanks = newRanks.reduce((a, b) => a + b, 0);

    rankingsTable.push({
      username,
      ranks: newRanks,
      sumOfRanks: newsumOfRanks,
      overallRank: null,
    });
  });

  let overallRank = 0;

  const speedrunResults = rankingsTable
    .sort((a, b) => a.sumOfRanks - b.sumOfRanks)
    .map((currInfo) => {
      overallRank += 1;
      return ({
        ...currInfo,
        overallRank,
      });
    });

  await db.close();
  res.json({
    speedrunResults,
    trackIds: allTracks,
    trackNames,
    maxRanks: rankingsForPlayersWithoutResults,
    trackDefaultLaps,
  });
};
