import sqlite3 from 'sqlite3';
import { open } from 'sqlite';
import SQL from 'sql-template-strings';
import getTrackInfo from '../../getTrackInfo';

export default async (req, res) => {
  const {
    query: {
      direction, trackId, laps, region, mode,
    },
  } = req;

  const db = await open({
    filename: process.env.dbLocation,
    driver: sqlite3.Database,
  });

  res.statusCode = 200;

  let table = process.env.allTable;
  if (region === 'AR') {
    table = process.env.arTable;
  } else if (region === 'BR') {
    table = process.env.brTable;
  } else if (region === 'MI') {
    table = process.env.miTable;
  }

  const modeArray = [];
  if (mode === 'normal' || mode === 'any') {
    modeArray.push('normal');
  }
  if (mode === 'timetrial' || mode === 'any') {
    modeArray.push('time-trial');
  }

  let directionArray = [];
  let lapsArray = [];

  if (direction === 'normal' || direction === 'reverse') {
    directionArray = [direction];
  } else {
    directionArray = ['normal', 'reverse'];
  }

  const lapsInt = Number.parseInt(laps, 10);
  if (lapsInt >= 1 && lapsInt <= 4) {
    lapsArray = [lapsInt];
  } else if (lapsInt === 0) {
    lapsArray = Array.from({ length: 21 }, (x, i) => i);
  } else if (lapsInt >= 5) {
    lapsArray = Array.from(new Array(16), (x, i) => i + 5);
  }

  const query = SQL`SELECT username AS recordHolder, min(result) as record, laps AS numLaps, reverse AS direction, row_number() OVER(ORDER BY result ASC) AS rank FROM `.append(table).append(` WHERE laps in (${lapsArray.join(', ')}) AND reverse in (${directionArray.map((d) => `'${d}'`).join(', ')}) AND mode in (${modeArray.map((m) => `'${m}'`).join(', ')}) AND venue IN ("${trackId}", "addon_${trackId}") GROUP BY username ORDER BY result ASC LIMIT 1000`);

  const records = JSON.parse(JSON.stringify(await db.all(query)));

  const isAddon = (await db.all(SQL`SELECT 1 FROM `.append(process.env.allTable).append(` WHERE venue = 'addon_${trackId}' LIMIT 1`))).length === 1;

  const trackInfo = await getTrackInfo(trackId, isAddon);
  await db.close();
  res.json({ records, trackInfo });
};
