/* eslint-disable no-await-in-loop */
import sqlite3 from 'sqlite3';
import { open } from 'sqlite';
import SQL from 'sql-template-strings';
import getTrackInfo from '../../getTrackInfo';
import { parseResult } from '../../apiHelper';

async function rowToRecord(result) {
  if (result.time == null) {
    return null;
  }
  const isAddon = result.venue.startsWith('addon_');
  const trackId = isAddon ? result.venue.slice(6) : result.venue;

  const formattedResult = parseResult(result.result);
  return {
    name: (await getTrackInfo(trackId, isAddon)).name,
    id: trackId,
    record: formattedResult,
    recordHolder: result.username,
    numLaps: Number.parseInt(result.laps, 10),
    direction: result.reverse === 'normal' ? 'normal' : 'al revés',
    isAddon,
    time: result.time.substring(0, 10),
  };
}

export default async (req, res) => {
  const db = await open({
    filename: process.env.dbLocation,
    driver: sqlite3.Database,
  });

  const {
    query: {
      direction, laps, region, mode,
    },
  } = req;

  let table = process.env.allTable;
  if (region === 'AR') {
    table = process.env.arTable;
  } else if (region === 'BR') {
    table = process.env.brTable;
  } else if (region === 'MI') {
    table = process.env.miTable;
  }

  const modeArray = [];
  if (mode === 'normal' || mode === 'any') {
    modeArray.push('normal');
  }
  if (mode === 'timetrial' || mode === 'any') {
    modeArray.push('time-trial');
  }

  let directionArray = [];
  let lapsArray = [];

  if (direction === 'normal' || direction === 'reverse') {
    directionArray = [direction];
  } else {
    directionArray = ['normal', 'reverse'];
  }

  const lapsInt = Number.parseInt(laps, 10);
  if (lapsInt >= 1 && lapsInt <= 4) {
    lapsArray = [lapsInt];
  } else if (lapsInt === 0) {
    lapsArray = Array.from({ length: 21 }, (x, i) => i);
  } else if (lapsInt >= 5) {
    lapsArray = Array.from(new Array(16), (x, i) => i + 5);
  }

  const query = SQL`select * from (SELECT username, venue, result, laps, reverse, time, row_number() OVER(PARTITION BY venue ORDER BY result ASC) AS place FROM `.append(table).append(` WHERE laps in (${lapsArray.join(', ')}) AND reverse in (${directionArray.map((d) => `'${d}'`).join(', ')}) AND mode in (${modeArray.map((m) => `'${m}'`).join(', ')})) as t where t.place = 1`);

  const unprocessedRecords = await db.all(query);

  const records = await Promise.all(unprocessedRecords.map(async (r) => rowToRecord(r)));

  await db.close();
  res.statusCode = 200;
  res.json({ records: records.filter((r) => r != null) });
};
