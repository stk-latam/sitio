import { withTranslation } from 'next-i18next';
import Link from 'next/link';
import React from 'react';
import { Button, Message } from 'semantic-ui-react';
import { proximoCampeonato } from './todosCampeonatos';

class NuevoCampeonatoBanner extends React.Component {
  render(): React.ReactNode {
    return proximoCampeonato() && (
    <Message
      color="purple"
    >
      {`Nuevo campeonato ${new Date(proximoCampeonato().fecha).toLocaleDateString()}`}
              &emsp;&emsp;
      <Link href={`/campeonatos/${proximoCampeonato().url}`}>
        <Button
          primary
          content="Más información"
        />
      </Link>
    </Message>
    );
  }
}
export default withTranslation('common')(NuevoCampeonatoBanner);
