# Español

Este sitio usa NodeJS. Se necesita `yarn` y `NodeJS`. Para iniciar el sitio:

`git clone https://gitlab.com/UnsolvedCypher/stk-latinoamerica`

`cd stk-latinoamerica`

`yarn install`

`yarn start`

# English

This site uses NodeJS. You need `yarn` and `NodeJS` installed. To start the site:

`git clone https://gitlab.com/UnsolvedCypher/stk-latinoamerica`

`cd stk-latinoamerica`

`yarn install`

`yarn start`