import fs from 'fs/promises';

import parser from 'xml2json';

export default async function getTrackInfo(trackId: string, isAddon: boolean) {
  const trackLocation = isAddon
    ? `${process.env.HOME}/.local/share/supertuxkart/addons/tracks/${trackId.replace('addon_', '')}`
    : `/usr/local/share/supertuxkart/data/tracks/${trackId}`;
  try {
    const contents = await fs.readFile(`${trackLocation}/track.xml`);
    const json = JSON.parse(await parser.toJson(contents));
    return {
      image: `trackLocation/${json.track.screenshot}`,
      name: json.track.name,
      defaultLaps: json.track['default-number-of-laps'],
    };
  } catch {
    return {
      name: trackId,
      defaultLaps: 3,
      image: null,
    };
  }
}
