import fetch from 'isomorphic-unfetch';

export default async function fetchUrl(url: string) {
  const fullUrl = `${process.env.NODE_ENV === 'development'
    ? 'http://localhost:3000'
    : 'https://stk.lat'
  }${url}`;
  return fetch(fullUrl);
}
